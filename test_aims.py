import aims

from nose.tools import assert_equal
def test_pos_ints():
    obs = aims.std([4,5,5,4,4,2,2,6])
    exp =  1.3228756555322954
    assert_equal(obs,exp)

def test_neg_ints():
    obs = aims.std([-3,-2,-4,-1,-4,-4])
    exp = 1.1547005383792515 
    assert_equal(obs,exp)

def test_pos_floats():
    obs = aims.std([2.1,3.5,1.3])
    exp = 0.9092121131323904
    assert_equal(obs,exp)

def test_floats():
    obs = aims.std([-2.1,3.5,-1.3])
    exp = 2.4729649321321876
    assert_equal(obs,exp)

def test_aims_std():
    obs = aims.std([])
    exp = 0.0
    assert_equal(obs, exp)

def test_avg_range1():
    files = ['data/bert/audioresult-00215']
    obs = aims.avg_range(files)
    exp = 5
    assert_equal(obs, exp)

def test_avg_range2():
    files = ['data/bert/audioresult-00557']
    obs = aims.avg_range(files)
    exp = 2
    assert_equal(obs, exp)

def test_avg_range3():
    files = ['data/bert/audioresult-00384']
    obs = aims.avg_range(files)
    exp = 10
    assert_equal(obs, exp)


