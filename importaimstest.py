#!/usr/bin/env python


def mean(data):
    """Return the sample arithmetic mean of data."""
    n = float(len(data))
    if n < 1:
    	raise ValueError('mean requires at least one data point')
    return sum(data)/n #

def _ss(data):
    """Return sum of square deviations of sequence data."""
    c = mean(data)
    print "mean", c
    ss = sum([(x-c)**2 for x in data])
    print "sums", ss
    return ss

def std(data):
    """Calculates the population standard deviation."""
    print "std"
    n = float(len(data))
    if n < 2:
       pvar =0
    else:
       ss = _ss(data)
       pvar = ss/n # the population variance
    return pvar

def main():
    print "Welcome to the AIMS module"
    
if __name__ == "__main__":
    main()




