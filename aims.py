#!/usr/bin/env python


def mean(data):
    """Return the sample arithmetic mean of data."""
    n = float(len(data))
    if n < 1:
    	raise ValueError('mean requires at least one data point')
    return sum(data)/n 

def _ss(data):
    """Return sum of square deviations of sequence data."""
    c = mean(data)
    ss = sum([(x-c)**2 for x in data])
    return ss

def std(data):
    """Calculates the population standard deviation."""
    n = float(len(data))
    if n < 2:
        return 0.0
        pvar =0
    else:
       ss = _ss(data)
       pvar = ss/n # the population variance
       std = pvar**0.5
    return std

def main():
    print "Welcome to the AIMS module"
    
if __name__ == "__main__":
    main()

def avg_range(x):
    """Calculates average range."""
    k = len(x)
    if k==0:
        return 0.0
    if type(x) != list:
        return "Input is not a list"
    newfile = open(x[0] , 'r')
    for line in newfile:
        line = line.strip()
        if line.startswith('Range'):
            line_list = line.split(':')
            return int(line_list[1])


